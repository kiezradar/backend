# Data import

Data is imported from many sources, the sources are defined in

	data-import/data/in/sources.json

Every source can be switched on or off with the `active` flag.

1. import the data from the sources with python

	``` shell
	cd data-import
	./import_data.sh
	```

	The import takes it's time, about one hour or so.

2. geolocate the data that has no geolocation

	1. start the nominatim server

		``` shell
		cd nominatim
		./run-nominatim.sh
		```

		The server creates it's own network, this is normal...

	2. start gelocating process

		``` shell
		cd data-import
		./geoloc_data.sh
		```

		This takes about 5 to 10 minutes.

	3. stop the nominatim server

		``` shell
		cd nominatim
		./stop-nominatim.sh
		```

		Don't forget this step, otherwise there is a dangling container on your system - you don't want that :smile:

3. check the data if needed

	Now the data resides in:

	```
	data-import/data/out
	data-import/data/out-geoloc
	```

4. [fill the database with new data](../40-database#fill-the-database-with-data)
