# API examples

Here are some examples for API calls with which you can start using the data.
The examples use <https://kiezradar.fokus.fraunhofer.de/api/> as base url, change accordingly to your server.

- [Swagger API definition](https://kiezradar.fokus.fraunhofer.de/api/)
- [PostgREST documentation](https://postgrest.org/)

## Basics

| Description | URL |
|--|--|
| API definition | https://kiezradar.fokus.fraunhofer.de/api/ |
| all events | https://kiezradar.fokus.fraunhofer.de/api/event |
| all meetings | https://kiezradar.fokus.fraunhofer.de/api/meeting |
| all event types | https://kiezradar.fokus.fraunhofer.de/api/eventtype |
| all sections | https://kiezradar.fokus.fraunhofer.de/api/section |

## Selection of returned values, ordering

| Description | URL |
|--|--|
| all events with all fields | <https://kiezradar.fokus.fraunhofer.de/api/event?select=*> |
| all events with title | https://kiezradar.fokus.fraunhofer.de/api/event?select=title |
| all events with title and start date | https://kiezradar.fokus.fraunhofer.de/api/event?select=title,periodstartdate |
| all events with title and start date, ordered by start date | https://kiezradar.fokus.fraunhofer.de/api/event?select=title,periodstartdate&order=periodstartdate |

## Filters

| Description | URL |
|--|--|
| current and future events, ordered by start date | https://kiezradar.fokus.fraunhofer.de/api/event?periodstartdate=gte.now&order=periodstartdate |
| current events only, ordered by title | https://kiezradar.fokus.fraunhofer.de/api/event?and=(periodstartdate.gte.now,periodenddate.lte.now)&order=title |
| current and future events of type "Beteiligungsprojekt", ordered by start date | https://kiezradar.fokus.fraunhofer.de/api/event?and=(periodstartdate.gte.now,eventtype.eq.84c62de9-ca70-5cda-a1a9-fcfac22c47f5)&order=periodstartdate |
| current and future events with location, ordered by start date | https://kiezradar.fokus.fraunhofer.de/api/event?and=(periodstartdate.gte.now,location.not.is.null)&order=periodstartdate |

## Joins

| Description | URL |
|--|--|
| current and future events with location, ordered by start date, with added event type and location | <https://kiezradar.fokus.fraunhofer.de/api/event?and=(periodstartdate.gte.now,location.not.is.null)&order=periodstartdate&select=*,location(coordinate,title),eventtype(title)> |
| current and future events, ordered by start date, with added district | <https://kiezradar.fokus.fraunhofer.de/api/event?and=(periodstartdate.gte.now)&order=periodstartdate&select=*,district(*)> |

## Pagination

| Description | URL |
|--|--|
| first 15 events (offset 0) | https://kiezradar.fokus.fraunhofer.de/api/event?limit=15 |
| first 15 events (offset 0) | https://kiezradar.fokus.fraunhofer.de/api/event?limit=15&offset=0 |
| 15 events, third page (offset 30) | https://kiezradar.fokus.fraunhofer.de/api/event?limit=15&offset=30 |

## Search

| Description | URL |
|--|--|
| events, whose title contains "SPD" | <https://kiezradar.fokus.fraunhofer.de/api/event?title=ilike.*spd*> |
| events, whose title ends with "SPD" | <https://kiezradar.fokus.fraunhofer.de/api/event?title=ilike.*spd> |

## Geolocation

| Description | URL |
|--|--|
| events within radius of 5km around the television tower | <https://kiezradar.fokus.fraunhofer.de/api/rpc/filter_distance_ev?distance=5000&lat=52.520794&lon=13.409371> |
| events within radius of 2km around Mauerpark | <https://kiezradar.fokus.fraunhofer.de/api/rpc/filter_distance_ev?distance=2000&lat=52.543366&lon=13.401904> |
