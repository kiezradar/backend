# Prepare your system

The backend relies heavily on docker.

We suggest developing on a Linux machine, as there were the least problems.

We ran into problems using docker on Windows for the nominatim server, all other images worked well with *Windows Subsystem for Linux*, too.

The manual show the according Linux commands, please adapt this to your operating system.

## Clone the repository

``` shell
git clone git@gitlab.com:kiezradar/backend.git
```


## Initialize the nominatim data for your region

In order to geolocate your data you have to fill the nominatim server with data of your region.

You cannot use the online nominatim server of OpenStreetmap, as this is not allowed for batch geolocation.

- <https://nominatim.openstreetmap.org/>

You can find data for the server at several places.
We used the Berlin data set of

- <https://download.geofabrik.de/europe/germany/>

The data sets are very, very big and take their time to be processed when initializing the nominatim image.
For the Berlin data of 75 MB the image initialization takes about 45 minutes.
We recommend to use the smallest data set possible for your purpose.

The used dataset is hardcoded in `nominatim/run-nominatim.sh`, change according to your region.

In order to initialize the server, call

``` shell
cd nominatim
./run-nominatim.sh
```

Wait for the server to initialize the database, if all goes well, there should be some line like:

``` shell
2022-03-10 00:26:20.573 UTC [41] LOG:  database system is ready to accept connections
```

Now you can stop the server with

``` shell
./stop-nominatim.sh
```

The database is persisted in the volume `nominatim-data`, so following calls of `run-nominatim.sh` don't take long.

!!! info
	Don't forget to stop the server, as it is started as background process.
	Stopping the server removes the docker network as well.



## Create the docker image

For *python*, *fmpp*, and *nominatim* we use images from DockerHub.

The image for postgres has to be created locally.

### postgres

This container has to be created after every data import and relies on the data file to be present.
Then, it is only a matter of calling the build script.

Given, this is rather complicated but this way every change of the database is reset when stopping and restarting the containers.
For the prototype this ensures that every database modification can be reset by a simple restart.

``` shell
cd docker-images/postgres
./build_image.sh
```

After building the image, check if there are any SQL errors when starting the container, otherwise your server crashes.
Here, you can get a feeling how long it takes the data to be loaded into the database, this is done every time the container is started.
That is why [the server](../50-server/) takes it's time for the first response: it waits for the database to be fully loaded.

``` shell
cd docker-images/postgres
./test_image.sh
```
