# KiezRadar-REST-Server

You followed every step, now it is time to start the server.

This really is the simplest step, just:

``` shell
cd server
./run.sh
```

Et voilà: call the API description or get all events:

- <http://localhost:3000/>
- <http://localhost:3000/event>

Do not despair if the first start takes a while, in the background all data is loaded into the database, then the server can access the database.
Please read "[the postgres docker image](../10-preparation/#postgres)" for an explanation.

## Restarting the server

If you have a running server, first stop the server, then restart it.

``` shell
cd server
./stop.sh
./run.sh
```
