# API development

We tried to enforce a API first approach when developing the backend.

Thus the central element of the API is the OpenAPI description of the API.

	openapi-spec/kiezradar.yaml

The API is used to generate the data model for the database and the REST API of KiezRadar.

After every API change, [recreate the database structure](../40-database#create-the-database-structure) and [refill the database with data](../40-database#fill-the-database-with-data).
