# Database creation

Data base creation is a two set process:

1. [create the database structure](#create-the-database-structure)
2. [fill the database with data](#fill-the-database-with-data)

Step one has to be repeated after every [change to the API](../20-api/).

Step two has to be repeated after every [data import](../30-data/).

You find the generated files in

	database-creation/generated

## Fill in your database credentials

You have to add your database credentials in two files:

1. `server/docker-compose.yml`
2. `database-creation/init-user-db.sh/init-user-db.sh.start`

The credentials are marked `<user>` and `<pwd>`.

!!! warning
	Don't commit/push files with your credentials filled in.

## Create the database structure

1. generate the JSON intermediate format from the API description
2. clean the generated model description
3. generate the database schema SQL code

``` shell
cd database-creation
./1_generate_db_models_json.sh
./2_clean_db_models_json.sh
./3_generate_db_schema_sql.sh
```

## Fill the database with data

1. merge all geolocated files into one
2. generate the SQL code for inserting the entries
3. create the `init-user-db.sh` file for initializing the database
4. [recreate the postgres docker image](../10-preparation/#postgres)

``` shell
cd database-creation
./4_merge_entries.sh
./5_generate_entries_sql.sh
./6_create_init-user-db.sh
```
