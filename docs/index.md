# Introduction

![Logo - stylized radar with text "KiezRadar"](images/logo.png){ align=right }

What is happening in my neighborhood?
What opportunities for participation are there in my neighborhood?
What is local politics dealing with?

Instead of digging through Berlin's digital offerings, the KiezRadar app will inform citizens proactively and in line with their needs about important events in politics and administration.
For example, users will be able to choose the topics or the radius for which they want to be informed.
In doing so, the information from other sources such as mein.Berlin or the council information system are used and prepared for the needs of the KiezRadar users.

---

This is the backend documentation, aimed at developers who want to understand how the backend works, who want to start the backend at their own servers, or who want to reuse code of the KiezRadar project.

The documentation focuses on how to use the code for your own server, in doing so, the main developerment aspects are explained.

---

If something seems very complicated (there are many scripts), there is a reason for most of the scripts and the task they are doing.
We try to document the whole process, but you can ask questions or open an issue anytime.

---

Errors, wishes
: https://gitlab.com/kiezradar/app/-/issues

Downloads/Releases
: https://gitlab.com/kiezradar/app/-/releases

Homepage
: https://kiezradar.fokus.fraunhofer.de
