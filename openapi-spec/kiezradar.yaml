openapi: 3.0.0
info:
  description: REST API für die KiezRadar-App
  version: 2.0.0
  title: KiezRadar API events
  contact:
    name: Fraunhofer Fokus
    url: 'https://kiezradar.fokus.fraunhofer.de/'
    email: kiezradar@fokus.fraunhofer.de
  license:
    name: Apache 2.0
    url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
paths:
  /events:
    get:
      tags:
        - Event
      summary: Returns all events.
      operationId: getEvents
      description: |-
        The content and filters can be queried using PostGREST queries, see http://postgrest.org/en/v7.0.0/api.html for reference.
        The query params contain examples but not the full potential of PostGREST queries.
      parameters:
        - in: query
          name: select
          description: 'content of the response, star "*" possible for all fields'
          schema:
            type: string
            minimum: 0
        - in: query
          name: limit
          description: maximum number of records to return
          schema:
            type: integer
            minimum: 0
        - in: query
          name: offset
          description: number of records to skip
          schema:
            type: integer
            minimum: 0
        - in: query
          name: order
          description: field to order by
          schema:
            type: string
      responses:
        '200':
          description: 'All events with their field, according to the given filters.'
          content:
            application/json;charset=UTF-8:
              schema:
                $ref: '#/components/schemas/Event'
        '400':
          description: Bad input parameter
  '/events/{eventId}':
    get:
      tags:
        - Event
      summary: return a specific event
      operationId: getEvent
      description: |
        Return the event which is identified by the specified eventId
      parameters:
        - name: eventId
          in: path
          required: true
          schema:
            type: string
      responses:
        '200':
          description: Specific event
          content:
            application/json;charset=UTF-8:
              schema:
                $ref: '#/components/schemas/Event'
        '404':
          description: Event not found
  '/meetings/{eventId}':
    get:
      tags:
        - Event
      summary: return a specific meeting
      operationId: getMeeting
      description: |
        Return the meeting which is identified by the specified eventId
      parameters:
        - name: eventId
          in: path
          required: true
          schema:
            type: string
      responses:
        '200':
          description: Specific meeting
          content:
            application/json;charset=UTF-8:
              schema:
                $ref: '#/components/schemas/Meeting'
        '404':
          description: Meeting not found
  '/participationprojects/{eventId}':
    get:
      tags:
        - Event
      summary: return a specific participation project
      operationId: getParticipationproject
      description: |
        Return the participation project which is identified by the specified eventId
      parameters:
        - name: eventId
          in: path
          required: true
          schema:
            type: string
      responses:
        '200':
          description: Specific participation project
          content:
            application/json;charset=UTF-8:
              schema:
                $ref: '#/components/schemas/ParticipationProject'
        '404':
          description: Participation project not found
  '/buildingprojects/{eventId}':
    get:
      tags:
        - Event
      summary: return a specific building project
      operationId: getBuildingproject
      description: |
        Return the building project which is identified by the specified eventId
      parameters:
        - name: eventId
          in: path
          required: true
          schema:
            type: string
      responses:
        '200':
          description: Specific building project
          content:
            application/json;charset=UTF-8:
              schema:
                $ref: '#/components/schemas/BuildingProject'
        '404':
          description: Building project not found
  '/neighborhoodcommunitymeetings/{eventId}':
    get:
      tags:
        - Event
      summary: return a specific neighborhood community meeting
      operationId: getNeighborhoodcommunitymeeting
      description: |
        Return the neighborhood community meeting which is identified by the specified eventId
      parameters:
        - name: eventId
          in: path
          required: true
          schema:
            type: string
      responses:
        '200':
          description: Specific neighborhood community meeting
          content:
            application/json;charset=UTF-8:
              schema:
                $ref: '#/components/schemas/NeighborhoodCommunityMeeting'
        '404':
          description: Neighborhood community meeting not found
  '/citizenssurveys/{eventId}':
    get:
      tags:
        - Event
      summary: return a specific citizens survey
      operationId: getCitizenssurvey
      description: |
        Return the citizens survey which is identified by the specified eventId
      parameters:
        - name: eventId
          in: path
          required: true
          schema:
            type: string
      responses:
        '200':
          description: Specific citizens survey
          content:
            application/json;charset=UTF-8:
              schema:
                $ref: '#/components/schemas/CitizensSurvey'
        '404':
          description: Citizens survey not found
servers:
  - url: 'https://kiezradar.fokus.fraunhofer.de/api/'
components:
  schemas:
    EventReducedInformation:
      title: EventReducedInformation
      type: object
      required:
        - uuid
        - title
        - eventType
        - section
        - periodStartDate
        - periodEndDate
        - district
        - image
        - apiLinks
        - createdAt
        - updatedAt
      properties:
        uuid:
          type: string
          format: uuid
          example: a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11
          x-primary-key: true
        title:
          type: string
          example: Musterevent
        eventType:
          title: EventType
          type: object
          anyOf:
            - $ref: '#/components/schemas/EventType'
        section:
          type: array
          items:
            $ref: '#/components/schemas/Section'
        periodStartDate:
          type: string
          format: date
          example: '2020-01-01'
        periodStartTime:
          type: string
          format: time
          example: '10:26:11'
        periodEndDate:
          type: string
          format: date
          example: '2020-03-03'
        periodEndTime:
          type: string
          format: time
          example: '10:26:11'
        location:
          title: Location
          anyOf:
            - $ref: '#/components/schemas/Location'
        district:
          title: District
          anyOf:
            - $ref: '#/components/schemas/District'
        description:
          type: string
          example: 'Informationen: Das Beteiligungsprojekt X im Bezirk Y möchte den Bürger*innen die Möglichkeit bieten, sich an Z zu beteiligen.'
        image:
          title: Image
          anyOf:
            - $ref: '#/components/schemas/FileLink'
        apiLinks:
          title: API-Links
          anyOf:
            - $ref: '#/components/schemas/Links'
        seriesUuid:
          type: string
          format: uuid
          description: 'If this event is part of a series, i.e. a recurring event, this is the uuid of the event series.'
          example: a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11
        createdAt:
          type: string
          format: date-time
          example: '2020-01-01T10:26:11'
        updatedAt:
          type: string
          format: date-time
          example: '2020-01-01T10:26:11'
    EventType:
      title: EventType
      type: object
      required:
        - uuid
        - title
        - createdAt
        - updatedAt
      properties:
        uuid:
          type: string
          format: uuid
          example: a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11
          x-primary-key: true
        title:
          type: string
          example: Beteiligungsverfahren
        createdAt:
          type: string
          format: date-time
          example: '2020-01-01T10:26:11'
        updatedAt:
          type: string
          format: date-time
          example: '2020-01-01T10:26:11'
    Links:
      title: Links
      type: object
      required:
        - uuid
        - linklist
      properties:
        uuid:
          type: string
          format: uuid
          example: a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11
          x-primary-key: true
        baseurl:
          type: string
          format: url
          example: 'https://kiezradar.fokus.fraunhofer.de/'
        linklist:
          type: array
          items:
            $ref: '#/components/schemas/Link'
          description: Linklist must contain at least one "self" link and one "list" link.
    Link:
      title: Link
      type: object
      required:
        - uuid
        - title
        - url
      properties:
        uuid:
          type: string
          format: uuid
          example: a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11
          x-primary-key: true
        title:
          type: string
          example: self or list or details
        url:
          type: string
          format: url
          example: '/event?uuid=eq.ee95387a-b21a-932e-9bc7-ca89bb56ad75 or http://restlos-gluecklich.berlin/'
    Event:
      title: Event
      allOf:
        - $ref: '#/components/schemas/EventReducedInformation'
        - type: object
          required:
            - source
          properties:
            source:
              type: string
              example: meinBerlin
            files:
              type: array
              items:
                $ref: '#/components/schemas/FileLink'
            moreLinks:
              title: more Links
              anyOf:
                - $ref: '#/components/schemas/Links'
    ParticipationProject:
      title: ParticipationProject
      allOf:
        - $ref: '#/components/schemas/Event'
        - type: object
          required:
            - projectAuthority
          properties:
            budget:
              type: string
              example: 300.000 €
              description: 'workaround: set type to string, parsing of given information is not possible'
            participationUrl:
              type: string
              format: url
              example: 'https://mein.berlin.de/projects/projectx/participate'
            projectAuthority:
              type: string
              example: Stadteilkoordination Mitte Zentrum - Spielplätze für Kinder e.V.
    BuildingProject:
      title: BuildingProject
      anyOf:
        - $ref: '#/components/schemas/ParticipationProject'
    NeighborhoodCommunityMeeting:
      title: NeighborhoodCommunityMeeting
      anyOf:
        - $ref: '#/components/schemas/Meeting'
    CitizensSurvey:
      title: CitizensSurvey
      allOf:
        - $ref: '#/components/schemas/Event'
        - type: object
          properties:
            participationUrl:
              type: string
              format: url
              example: 'https://mein.berlin.de/projects/projectx/participate'
            projectAuthority:
              type: string
              example: Bezirksamt Charlottenburg-Wilmersdorf
    Meeting:
      title: Meeting
      allOf:
        - $ref: '#/components/schemas/Event'
        - type: object
          properties:
            agendaItems:
              type: array
              items:
                $ref: '#/components/schemas/AgendaItem'
            participants:
              type: array
              items:
                $ref: '#/components/schemas/Person'
            invitation:
              title: Invitation
              anyOf:
                - $ref: '#/components/schemas/FileLink'
            resultsProtocol:
              title: ResultsProtocol
              anyOf:
                - $ref: '#/components/schemas/FileLink'
    Section:
      title: Section
      type: object
      required:
        - uuid
        - title
        - createdAt
        - updatedAt
      properties:
        uuid:
          type: string
          format: uuid
          example: a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11
          x-primary-key: true
        title:
          type: string
          example: Bauen & Wohnen
        createdAt:
          type: string
          format: date-time
          example: '2020-01-01T10:26:11'
        updatedAt:
          type: string
          format: date-time
          example: '2020-01-01T10:26:11'
    Location:
      title: Location
      type: object
      required:
        - uuid
        - title
        - createdAt
        - updatedAt
      properties:
        uuid:
          type: string
          format: uuid
          example: a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11
          x-primary-key: true
        title:
          type: string
          example: 'Treffpunkt Mühle Marzahn'
        description:
          type: string
          example: 'Bachstraße 1, 3. Hinterhaus rechts'
        geocodingtitle:
          type: string
          example: 'Bachstraße 1'
        coordinate:
          type: string
          format: geometry
          example: 'LINESTRING(0 0, 1 1, 2 1, 2 2)'
        createdAt:
          type: string
          format: date-time
          example: '2020-01-01T10:26:11'
        updatedAt:
          type: string
          format: date-time
          example: '2020-01-01T10:26:11'
    AgendaItem:
      title: AgendaItem
      type: object
      required:
        - uuid
        - title
        - createdAt
        - updatedAt
      properties:
        uuid:
          type: string
          format: uuid
          example: a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11
          x-primary-key: true
        title:
          type: string
          example: 'TOP1: Beschluss X'
        sortorder:
          type: integer
          example: 3
        public:
          type: boolean
        auxiliaryFile:
          type: array
          items:
            $ref: '#/components/schemas/FileLink'
        createdAt:
          type: string
          format: date-time
          example: '2020-01-01T10:26:11'
        updatedAt:
          type: string
          format: date-time
          example: '2020-01-01T10:26:11'
    District:
      title: District
      type: object
      required:
        - uuid
        - title
        - location
        - createdAt
        - updatedAt
      properties:
        uuid:
          type: string
          format: uuid
          example: a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11
          x-primary-key: true
        title:
          type: string
          example: Friedrichshain-Kreuzberg
        location:
          title: Location
          anyOf:
            - $ref: '#/components/schemas/Location'
        createdAt:
          type: string
          format: date-time
          example: '2020-01-01T10:26:11'
        updatedAt:
          type: string
          format: date-time
          example: '2020-01-01T10:26:11'
    FileLink:
      title: FileLink
      type: object
      required:
        - uuid
        - title
        - filename
        - url
        - createdAt
        - updatedAt
      properties:
        uuid:
          type: string
          format: uuid
          example: a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11
          x-primary-key: true
        title:
          type: string
          example: Einladung_zur_Ausschusssitzung
        filename:
          type: string
          example: Einladung_zur_Ausschusssitzung.pdf
        mimeType:
          type: string
          example: application/pdf
        url:
          type: string
          format: url
          pattern: '^https\\:\\/\\/kiezradar\\.fokus\\.fraunhofer\\.de\\/api\\/locations$'
        size:
          type: integer
          example: 82930
          description: 'workaround: set type to integer; initial property description: type: number, format: int32'
        sha512Checksum:
          type: string
          example: 070fa3584df96dde9725f1fe9a99376abbc6ecd5dabc67d006f22b7d239e725f98608df97dde692412bf8a6812241e09cf75f614e2314a3de1a2af8c7d6e4aa1
          description: SHA 512 checksum of image file
        createdAt:
          type: string
          format: date-time
          example: '2020-01-01T10:26:11'
        updatedAt:
          type: string
          format: date-time
          example: '2020-01-01T10:26:11'
    Person:
      title: Person
      type: object
      required:
        - uuid
        - name
        - createdAt
        - updatedAt
      properties:
        uuid:
          type: string
          format: uuid
          example: a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11
          x-primary-key: true
        name:
          type: string
          example: Marianne Musterfrau
        status:
          type: array
          items:
            $ref: '#/components/schemas/PersonStatus'
        createdAt:
          type: string
          format: date-time
          example: '2020-01-01T10:26:11'
        updatedAt:
          type: string
          format: date-time
          example: '2020-01-01T10:26:11'
    PersonStatus:
      title: PersonStatus
      type: object
      required:
        - uuid
        - title
        - createdAt
        - updatedAt
      properties:
        uuid:
          type: string
          format: uuid
          example: a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11
          x-primary-key: true
        title:
          type: string
          example: Bezirksverordnete
        createdAt:
          type: string
          format: date-time
          example: '2020-01-01T10:26:11'
        updatedAt:
          type: string
          format: date-time
          example: '2020-01-01T10:26:11'
    Organization:
      title: Organization
      type: object
      required:
        - uuid
      properties:
        uuid:
          type: string
          format: uuid
          example: a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11
          x-primary-key: true
    Body:
      title: Body
      type: object
      properties:
        uuid:
          type: string
          format: uuid
          example: a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11
          x-primary-key: true
        id:
          type: string
          format: url
        name:
          type: string
          example: Bezirk Mitte
        type:
          type: string
          pattern: '^https\\:\\/\\/kiezradar\\.fokus\\.fraunhofer\\.de\\/api\\/locations$'
        website:
          type: string
          format: url
          example: 'https://www.berlin.de/ba-mitte/'
        locationUuid:
          type: string
          format: uuid
          example: a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11
        location:
          title: Location
          anyOf:
            - $ref: '#/components/schemas/Location'
          description: 'workaround: defined locationUuid as a new property; added title, anyOf as property descriptions for this property to enable description'
