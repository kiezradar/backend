#!/bin/bash

source ../../variables.sh

CONTAINERNAME=$CONTAINER_DB
IMAGENAME=$IMAGE_DB

echo "Build image $IMAGENAME"
echo

cp ../../$DB_CREATION_PATH/$TEMP_PATH/$FILE_INIT .
docker build --tag $IMAGENAME .
rm $FILE_INIT
