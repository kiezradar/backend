#!/bin/bash

source ../../variables.sh

CONTAINERNAME=$CONTAINER_DB
IMAGENAME=$IMAGE_DB

docker run --rm --name $CONTAINERNAME --interactive --tty --env POSTGRES_PASSWORD=example $IMAGENAME
