#!/bin/bash

source ../variables.sh
source variables.sh

CONTAINERNAME=$CONTAINER_FMPP
IMAGENAME=$IMAGE_FMPP
WORKDIR=/local

SOURCEFILE=$TEMP_FILE_API_JSON_CLEAN
TEMPLATEPATH=db_templates
OUTPATH=$TEMP_PATH

echo "Generate postgres SQL schema."
echo

docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--interactive \
	--tty \
	--name $CONTAINERNAME \
	--volume "${PWD}":$WORKDIR \
	$IMAGENAME \
		--data="json(/local/$SOURCEFILE)" \
		--source-root=/local/$TEMPLATEPATH \
		--output-root=/local/$OUTPATH \
		--output-encoding=utf8
