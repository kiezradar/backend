CONTAINER_OPENAPI_GEN=openapi-generator

TEMP_FILE_API_JSON=$TEMP_PATH/api_json.json
TEMP_FILE_API_JSON_CLEAN=$TEMP_PATH/api_json_clean.json

EVENTS=events
DISTRICTS=districts
EVENTTYPES=eventtypes
SECTIONS=sections

SQL_SCHEMA=create_db.sql
