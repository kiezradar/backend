<#list districts as district, details>

	<@compress single_line=true>
	INSERT INTO public.Location (
		coordinate,
		createdAt,
		description,
		title,
		updatedAt,
		uuid
	) VALUES (
		<#if (details.geoloc.type == "Polygon")>
			'SRID=4326;${details.geoloc.type?upper_case}(
				<#list details.geoloc.coordinates as colist1>
					(
						<#list colist1 as coordinate>
							${coordinate[1]} ${coordinate[0]}
							<#sep>, </#sep>
						</#list>
					)
					<#sep>, </#sep>
				</#list>
			)',
		</#if>
		<#if (details.geoloc.type == "MultiPolygon")>
			'SRID=4326;${details.geoloc.type?upper_case}(
				<#list details.geoloc.coordinates as colist1>
					(
						<#list colist1 as colist2>
							(
								<#list colist2 as coordinate>
									${coordinate[1]} ${coordinate[0]}
									<#sep>, </#sep>
								</#list>
							)
							<#sep>, </#sep>
						</#list>
					)
					<#sep>, </#sep>
				</#list>
			)',
		</#if>
		'${.now?string["yyyy-MM-dd HH:mm:ss"]}',
		NULL,
		'${district}',
		'${.now?string["yyyy-MM-dd HH:mm:ss"]}',
		'${details.uuid}'
	);
	</@compress>

	<@compress single_line=true>
	INSERT INTO public.District (
		createdAt,
		location,
		title,
		updatedAt,
		uuid
	) VALUES (
		'${.now?string["yyyy-MM-dd HH:mm:ss"]}',
		'${details.uuid}',
		'${district}',
		'${.now?string["yyyy-MM-dd HH:mm:ss"]}',
		'${details.uuid}'
	);
	</@compress>

</#list>
