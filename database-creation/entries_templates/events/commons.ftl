<#macro escapeText theText><@compress single_line=true>
	${theText?replace("'", "''")}
</@compress></#macro>


<#macro getLocation entry><@compress single_line=true>
	<#if (entry.kiezradar.location??)>
		INSERT INTO public.Location (
			coordinate,
			createdAt,
			description,
			geocodingtitle,
			title,
			updatedAt,
			uuid
		) VALUES (
			<#if (entry.kiezradar.location.geometry??)>'SRID=4326;POINT(${entry.kiezradar.location.geometry.coordinates.lat} ${entry.kiezradar.location.geometry.coordinates.lon})'<#else>NULL</#if>,
			'${entry.kiezradar.location.createdat}',
			<#if (entry.kiezradar.location.description??)>'<@com.escapeText entry.kiezradar.location.description />'<#else>NULL</#if>,
			<#if (entry.kiezradar.location.geocodingtitle??)>'<@com.escapeText entry.kiezradar.location.geocodingtitle />'<#else>NULL</#if>,
			'<@com.escapeText entry.kiezradar.location.title />',
			'${entry.kiezradar.location.updatedat}',
			'${entry.kiezradar.uuid}'
		);
	</#if>
</@compress></#macro>

<#macro getImage entry><@compress single_line=true>
	<#if (entry.kiezradar.image??)>
		INSERT INTO public.FileLink (
			createdAt,
			filename,
			mimeType,
			sha512Checksum,
			size,
			title,
			updatedAt,
			url,
			uuid
		) VALUES (
			'${entry.kiezradar.image.createdat}',
			'${entry.kiezradar.image.filename}',
			<#if entry.kiezradar.image.mimetype??>'${entry.kiezradar.image.mimetype}'<#else>NULL</#if>,
			<#if entry.kiezradar.image.sha512checksum??>'${entry.kiezradar.image.sha512checksum}'<#else>NULL</#if>,
			<#if entry.kiezradar.image.size??>'${entry.kiezradar.image.size}'<#else>NULL</#if>,
			'<@com.escapeText entry.kiezradar.image.title />',
			'${entry.kiezradar.image.updatedat}',
			'${entry.kiezradar.image.url}',
			'${entry.kiezradar.image.uuid}'
		);
	</#if>
</@compress></#macro>

<#macro createLinkList entry>

	<#list ["apilinks", "morelinks"] as linklisttitle>

		<#if entry.kiezradar[linklisttitle]??>

			<@compress single_line=true>
			INSERT INTO public.Links (
				baseurl,
				uuid
			) VALUES (
				<#if (linklisttitle == "apilinks")>'${entry.kiezradar[linklisttitle].api_url}'<#else>NULL</#if>,
				'${entry.kiezradar[linklisttitle].uuid}'
			);
			</@compress>

			<#list entry.kiezradar[linklisttitle].links as title, link>

				<@compress single_line=true>
				INSERT INTO public.Link (
					title,
					url,
					uuid
				) VALUES (
					'${title}',
					'${link.url}',
					'${link.uuid}'
				) ON CONFLICT DO NOTHING;
				</@compress>

				<@createNMInsert "Links", "Link", entry.kiezradar[linklisttitle].uuid, link.uuid />

			</#list>

		</#if>

	</#list>

</#macro>


<#macro createSectionLinks entry, slots>

	<#list entry.kiezradar.sections as section_uuid>
		<#list slots as slot>
			<@createNMInsert slot, "Section", entry.kiezradar.uuid, section_uuid />
		</#list>
	</#list>

</#macro>


<#macro createNMInsert slotn, slotm, valuen, valuem>

	<@compress single_line=true>
	INSERT INTO public.nm${slotn}${slotm} (
		fk_${slotn}_uuid,
		fk_${slotm}_uuid
	) VALUES (
		'${valuen}',
		'${valuem}'
	);
	</@compress>

</#macro>
