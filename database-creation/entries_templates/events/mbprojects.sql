<#import "commons.ftl" as com>

<#list mbprojects as entry>

	<#assign apiLinks><#if entry.kiezradar.apilinks??>'${entry.kiezradar.apilinks.uuid}'<#else>NULL</#if></#assign>
	<#assign createdAt>'${entry.kiezradar.createdat}'</#assign>
	<#assign description><#if (entry.kiezradar.description??)>'<@com.escapeText entry.kiezradar.description />'<#else>NULL</#if></#assign>
	<#assign district>'${entry.kiezradar.district}'</#assign>
	<#assign eventType>'${entry.kiezradar.eventtype_uuid}'</#assign>
	<#assign image><#if (entry.kiezradar.image??)>'${entry.kiezradar.uuid}'<#else>'${entry.kiezradar.eventtype_uuid}'</#if></#assign>
	<#assign location><#if (entry.kiezradar.location??)>'${entry.kiezradar.uuid}'<#else>NULL</#if></#assign>
	<#assign periodStartDate>'${entry.kiezradar.startdate}'</#assign>
	<#assign periodStartTime><#if entry.kiezradar.starttime??>'${entry.kiezradar.starttime}'<#else>NULL</#if></#assign>
	<#assign periodEndDate>'${entry.kiezradar.enddate}'</#assign>
	<#assign periodEndTime><#if entry.kiezradar.endtime??>'${entry.kiezradar.endtime}'<#else>NULL</#if></#assign>
	<#assign seriesUuid><#if entry.kiezradar.series??>'${entry.kiezradar.series}'<#else>NULL</#if></#assign>
	<#assign title>'<@com.escapeText entry.kiezradar.title />'</#assign>
	<#assign updatedAt>'${entry.kiezradar.updatedat}'</#assign>
	<#assign uuid>'${entry.kiezradar.uuid}'</#assign>

	<#assign moreLinks><#if entry.kiezradar.morelinks??>'${entry.kiezradar.morelinks.uuid}'<#else>NULL</#if></#assign>
	<#assign source>'${entry.kiezradar.source_title}'</#assign>

	<#assign budget><#if entry.kiezradar.budget??>'${entry.kiezradar.budget}'<#else>NULL</#if></#assign>
	<#assign participationUrl><#if entry.kiezradar.participationurl??>'${entry.kiezradar.participationurl}'<#else>NULL</#if></#assign>
	<#assign projectAuthority>'${entry.kiezradar.projectauthority}'</#assign>

	<@com.createLinkList entry />
	<@com.getImage entry />
	<@com.getLocation entry />

	<@compress single_line=true>
	INSERT INTO public.EventReducedInformation (
		apiLinks,
		createdAt,
		description,
		district,
		eventType,
		image,
		location,
		periodEndDate,
		periodEndTime,
		periodStartDate,
		periodStartTime,
		seriesUuid,
		title,
		updatedAt,
		uuid
	) VALUES (
		${apiLinks},
		${createdAt},
		${description},
		${district},
		${eventType},
		${image},
		${location},
		${periodEndDate},
		${periodEndTime},
		${periodStartDate},
		${periodStartTime},
		${seriesUuid},
		${title},
		${updatedAt},
		${uuid}
	);
	</@compress>

	<@compress single_line=true>
	INSERT INTO public.Event (
		apiLinks,
		createdAt,
		description,
		district,
		eventType,
		image,
		location,
		moreLinks,
		periodEndDate,
		periodEndTime,
		periodStartDate,
		periodStartTime,
		seriesUuid,
		source,
		title,
		updatedAt,
		uuid
	) VALUES (
		${apiLinks},
		${createdAt},
		${description},
		${district},
		${eventType},
		${image},
		${location},
		${moreLinks},
		${periodEndDate},
		${periodEndTime},
		${periodStartDate},
		${periodStartTime},
		${seriesUuid},
		${source},
		${title},
		${updatedAt},
		${uuid}
	);
	</@compress>

	<@compress single_line=true>
	INSERT INTO public.ParticipationProject (
		apiLinks,
		budget,
		createdAt,
		description,
		district,
		eventType,
		image,
		location,
		moreLinks,
		participationUrl,
		periodEndDate,
		periodEndTime,
		periodStartDate,
		periodStartTime,
		projectAuthority,
		seriesUuid,
		source,
		title,
		updatedAt,
		uuid
	) VALUES (
		${apiLinks},
		${budget},
		${createdAt},
		${description},
		${district},
		${eventType},
		${image},
		${location},
		${moreLinks},
		${participationUrl},
		${periodEndDate},
		${periodEndTime},
		${periodStartDate},
		${periodStartTime},
		${projectAuthority},
		${seriesUuid},
		${source},
		${title},
		${updatedAt},
		${uuid}
	);
	</@compress>

	<@com.createSectionLinks entry, ["EventReducedInformation", "Event", "ParticipationProject"] />

</#list>
