<#list eventtypes as eventtype, details>

	<@compress single_line=true>
	INSERT INTO public.EventType (
		createdAt,
		title,
		updatedAt,
		uuid
	) VALUES (
		'${.now?string["yyyy-MM-dd HH:mm:ss"]}',
		'${eventtype}',
		'${.now?string["yyyy-MM-dd HH:mm:ss"]}',
		'${details.uuid}'
	) ON CONFLICT DO NOTHING;
	</@compress>

	<@compress single_line=true>
	INSERT INTO public.FileLink (
		createdAt,
		filename,
		mimeType,
		sha512Checksum,
		size,
		title,
		updatedAt,
		url,
		uuid
	) VALUES (
		'${.now?string["yyyy-MM-dd HH:mm:ss"]}',
		'${details.filename}',
		'${details.mimetype}',
		'${details.sha512checksum}',
		${details.size},
		'Platzhalter ${eventtype} />',
		'${.now?string["yyyy-MM-dd HH:mm:ss"]}',
		'${details.url}',
		'${details.uuid}'
	) ON CONFLICT DO NOTHING;
	</@compress>

</#list>
