<#list sections as section, details>

	<@compress single_line=true>
	INSERT INTO public.Section (
		createdAt,
		title,
		updatedAt,
		uuid
	) VALUES (
		'${.now?string["yyyy-MM-dd HH:mm:ss"]}',
		'${section}',
		'${.now?string["yyyy-MM-dd HH:mm:ss"]}',
		'${details.uuid}'
	);
	</@compress>

</#list>
