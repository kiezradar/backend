<#list models as model, vars>

	CREATE TABLE public.${model} (
		<#list vars as name, var_desc>
			<@compress single_line=true>
				${name}
				${var_desc.dataType}
				<#if var_desc.required>NOT NULL</#if>
				<#if (var_desc.defaultValue??)> DEFAULT ${var_desc.defaultValue}</#if>
				<#if var_desc.primaryKey>PRIMARY KEY</#if>
				<#sep>,</#sep>
			</@compress>

		</#list>
	) WITH (oids = false);

</#list>

<#list models as model, vars>
	<#list vars as name, var_desc>
		<#if (var_desc.reference??)>

			ALTER TABLE public.${model} ADD FOREIGN KEY (${name}) REFERENCES public.${var_desc.reference};

		</#if>
	</#list>
</#list>
