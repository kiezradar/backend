#!/bin/bash

source ../variables.sh
source variables.sh

CONTAINERNAME=$CONTAINER_PYTHON
IMAGENAME=$IMAGE_PYTHON
WORKDIR=/usr/src/myapp

echo "Merge data files."
echo

INPUT=$DATA_DIR/$DATA_PATH/$DATA_PATH_OUT_GEOLOC
INPUT_JSON=$DATA_DIR/$DATA_PATH/$DATA_PATH_OUT/*.json
OUTPATH=$TEMP_PATH/$DATA_PATH/$DATA_PATH_IN
OUTFILE=database-creation/$OUTPATH/data.json

# merge data files
docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--interactive \
	--tty \
	--name $CONTAINERNAME \
	--volume "${PWD}/../":$WORKDIR \
	--workdir $WORKDIR \
	$IMAGENAME \
		database-creation/data-merge-scripts/data_merge.py \
			--input $INPUT \
			--output $OUTFILE

# copy simple json files
cp ../$INPUT_JSON $OUTPATH
