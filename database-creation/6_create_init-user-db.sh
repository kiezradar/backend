#!/bin/bash

source ../variables.sh
source variables.sh

OUTFILE=$TEMP_PATH/$FILE_INIT

rm $OUTFILE
cat \
	$FILE_INIT/$FILE_INIT.start \
	$TEMP_PATH/$SQL_SCHEMA \
	$FILE_INIT/$FILE_INIT.views \
	$TEMP_PATH/$DATA_PATH/$DATA_PATH_OUT/*.sql \
	$TEMP_PATH/$DATA_PATH/$DATA_PATH_OUT/$EVENTS/*.sql \
	$FILE_INIT/$FILE_INIT.end \
		>> $OUTFILE
