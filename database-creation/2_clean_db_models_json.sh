#!/bin/bash

source ../variables.sh
source variables.sh

CONTAINERNAME=$CONTAINER_PYTHON
IMAGENAME=$IMAGE_PYTHON

WORKDIR=/usr/src/myapp

INPUT=$TEMP_FILE_API_JSON
OUTFILE=$TEMP_FILE_API_JSON_CLEAN

echo "Read models file and clean it resp. add relations."
echo

docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--interactive \
	--tty \
	--name $CONTAINERNAME \
	--volume "${PWD}":$WORKDIR \
	--workdir $WORKDIR \
	$IMAGENAME \
		clean-db-scripts/clean_db.py \
			--input $INPUT \
			--output $OUTFILE
