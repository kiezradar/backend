CREATE FUNCTION filter_distance_evri(distance INTEGER, lat FLOAT, lon FLOAT)
  RETURNS SETOF EventReducedInformation
  AS '
    SELECT
      ev.*
    FROM
      EventReducedInformation ev
      JOIN Location loc ON ev.location = loc.uuid
    WHERE
      ST_DWithin(
        ST_Transform(ST_SetSRID(loc.coordinate::geometry, 4326), 3857),
        ST_Transform(ST_SetSRID(format(''SRID=4326;POINT(%s %s)'', lat, lon)::geometry, 4326), 3857),
        distance
      )
    ;
  '
  LANGUAGE SQL
  ;

CREATE FUNCTION filter_distance_ev(distance INTEGER, lat FLOAT, lon FLOAT)
  RETURNS SETOF Event
  AS '
    SELECT
      ev.*
    FROM
      Event ev
      JOIN Location loc ON ev.location = loc.uuid
    WHERE
      ST_DWithin(
        ST_Transform(ST_SetSRID(loc.coordinate::geometry, 4326), 3857),
        ST_Transform(ST_SetSRID(format(''SRID=4326;POINT(%s %s)'', lat, lon)::geometry, 4326), 3857),
        distance
      )
    ;
  '
  LANGUAGE SQL
  ;

-- CREATE FUNCTION filter_distance_simple(distance INTEGER)
--   RETURNS SETOF EventReducedInformation
--   AS '
--     SELECT
--       ev.*
--     FROM
--       EventReducedInformation ev
--       JOIN Location loc ON ev.location = loc.uuid
--     WHERE
--       ST_DWithin(
--         ST_Transform(ST_SetSRID(loc.coordinate::geometry, 4326), 3857),
--         ST_Transform(ST_SetSRID(''SRID=4326;POINT(52.471645 13.328476)''::geometry, 4326), 3857),
--         distance
--       )
--     ;
--   '
--   LANGUAGE SQL
--   ;
--
-- CREATE FUNCTION show_distance()
--   RETURNS FLOAT
--   AS '
--     SELECT ST_Distance(
--       ST_Transform(''SRID=4326;POINT(52.471645 13.328476)'', 3857),
--       ST_Transform(''SRID=4326;POINT(52.469449 13.541079)'', 3857)
--     );
--   '
--   LANGUAGE SQL
--   IMMUTABLE;
--
-- CREATE FUNCTION distance1()
--   RETURNS BOOLEAN
--   AS '
--     SELECT ST_DWithin(
--       ST_Transform(''SRID=4326;POINT(52.471645 13.328476)'', 3857),
--       ST_Transform(''SRID=4326;POINT(52.469449 13.541079)'', 3857),
--       25000
--     );
--   '
--   LANGUAGE SQL
--   IMMUTABLE;
--
-- CREATE FUNCTION distance2()
--   RETURNS BOOLEAN
--   AS '
--     SELECT ST_DWithin(
--       ST_Transform(''SRID=4326;POINT(52.471645 13.328476)'', 3857),
--       ST_Transform(''SRID=4326;POINT(52.469449 13.541079)'', 3857),
--       24000
--     );
--   '
--   LANGUAGE SQL
--   IMMUTABLE;
--
-- CREATE FUNCTION filter_eventreducedinformation_section(sections UUID[])
--   RETURNS SETOF EventReducedInformation
--   AS '
--     SELECT
--       ev.*
--     FROM
--       EventReducedInformation ev
--       JOIN nmEventReducedInformationSection nmes ON ev.uuid = nmes.fk_EventReducedInformation_uuid
--     WHERE
--       nmes.fk_Section_uuid IN sections
--     ;
--   ' LANGUAGE SQL;
