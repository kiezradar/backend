#!/bin/bash

source ../variables.sh
source variables.sh

CONTAINERNAME=$CONTAINER_OPENAPI_GEN
IMAGENAME=$IMAGE_OPENAPI

OUTPATH=$TEMP_PATH

THIS_PATH=database-creation

echo "Generate temporary database models from API in JSON"
echo

docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--interactive \
	--tty \
	--name $CONTAINERNAME \
	--volume "${PWD}/../:/local" \
	$IMAGENAME \
		generate \
			--input-spec /local/$OPENAPI_SPEC \
			--generator-name markdown \
			--output /local/$THIS_PATH/$OUTPATH \
			--template-dir /local/$THIS_PATH/generator-templates/

#--global-property debugModels=true

# removing additional files of generation because of using predefined markdown generator
rm -r $OUTPATH/Apis
rm -r $OUTPATH/Models
rm -r $OUTPATH/.openapi-generator
rm $OUTPATH/.openapi-generator-ignore

# renaming result file
mv $OUTPATH/README.md $TEMP_FILE_API_JSON
