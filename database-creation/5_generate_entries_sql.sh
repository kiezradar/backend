#!/bin/bash

source ../variables.sh
source variables.sh

echo "Generate entries SQL schema."
echo

IMAGENAME=$IMAGE_FMPP
CONTAINERNAME=$CONTAINER_FMPP
WORKDIR=/local

OUTPATH=$TEMP_PATH/$DATA_PATH/$DATA_PATH_IN
SOURCEFILE=$OUTPATH/data.json
TEMPLATEPATH=entries_templates

DATA_OUT=$TEMP_PATH/$DATA_PATH/$DATA_PATH_OUT

rm --recursive $DATA_OUT/*

docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--interactive \
	--tty \
	--name $CONTAINERNAME \
	--volume "${PWD}":$WORKDIR \
	$IMAGENAME \
		--data="json(/local/$OUTPATH/$DISTRICTS.json)" \
		--source-root=/local/$TEMPLATEPATH/$DISTRICTS \
		--output-root=/local/$DATA_OUT \
		--output-encoding=utf8 \
		--modes="ignore(*.ftl)"

docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--interactive \
	--tty \
	--name $CONTAINERNAME \
	--volume "${PWD}":$WORKDIR \
	$IMAGENAME \
		--data="json(/local/$OUTPATH/$SECTIONS.json)" \
		--source-root=/local/$TEMPLATEPATH/$SECTIONS \
		--output-root=/local/$DATA_OUT \
		--output-encoding=utf8 \
		--modes="ignore(*.ftl)"

docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--interactive \
	--tty \
	--name $CONTAINERNAME \
	--volume "${PWD}":$WORKDIR \
	$IMAGENAME \
		--data="json(/local/$OUTPATH/$EVENTTYPES.json)" \
		--source-root=/local/$TEMPLATEPATH/$EVENTTYPES \
		--output-root=/local/$DATA_OUT \
		--output-encoding=utf8 \
		--modes="ignore(*.ftl)"

docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--interactive \
	--tty \
	--name $CONTAINERNAME \
	--volume "${PWD}":$WORKDIR \
	$IMAGENAME \
		--data="json(/local/$SOURCEFILE)" \
		--source-root=/local/$TEMPLATEPATH/$EVENTS \
		--output-root=/local/$DATA_OUT/$EVENTS/ \
		--output-encoding=utf8 \
		--modes="ignore(*.ftl)"
