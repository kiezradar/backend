import argparse
import json
import os
import traceback

# merge all model files
def merge_data(source_path, is_verbose):

    merged_data = {}

    input_path = os.path.join(os.getcwd(), source_path)
    data_types = []

    for name in os.listdir(input_path):
        if os.path.isdir(os.path.join(input_path, name)):
            data_types.append(name)

    for data_type in data_types:

        merged_data[data_type] = []
        data_type_dir = os.path.join(input_path, data_type)

        for entry_file in os.listdir(data_type_dir):

            entry_file_name = os.path.join(data_type_dir, entry_file)
            if (is_verbose):
                print("Processing '{}'.".format(entry_file_name))

            with open(entry_file_name, 'r') as file:
                merged_data[data_type].append(json.load(file))

    return merged_data

# save json file
def save_json_file(json_data, filename):

    output_file = os.path.join(os.getcwd(), filename)

    print("Saving to '{}'".format(output_file))
    os.makedirs(os.path.dirname(output_file), exist_ok=True)
    with open(output_file, 'w', encoding='utf-8') as outfile:
        json.dump(json_data, outfile, sort_keys=True, indent=4)

    return

# init args parser, read and save sources
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description = "Merge data files.")
    parser.add_argument("-i", "--input", type = str, help = "input path", required = True)
    parser.add_argument("-o", "--output", type = str, help = "output file", required = True)
    parser.add_argument("-v", "--verbose", action='store_true', help = "verbose output", required = False)

    args = parser.parse_args()

    try:

        merged_data = merge_data(args.input, args.verbose)
        save_json_file(merged_data, args.output)

    except Exception as e:
        print()
        print("Error {}: {}".format(type(e).__name__, e))
        print(traceback.format_exc())
