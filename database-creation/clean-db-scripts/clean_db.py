import argparse
import json
import os
import traceback

from clean_model import clean_model

# read and return api model
def read_model(source_file):

    input_file = os.path.join(os.getcwd(), source_file)

    print("Reading API model from '{}'...".format(input_file))

    if not os.path.isfile(input_file):
        raise FileNotFoundError("File '{}' does not exist.".format(input_file))

    with open(input_file, 'r') as file:
        api_model = json.load(file)

    print("API model read.")
    print()

    return api_model

# save json file
def save_json_file(json_data, filename):

    output_file = os.path.join(os.getcwd(), filename)

    print("Saving to '{}'".format(output_file))
    os.makedirs(os.path.dirname(output_file), exist_ok=True)
    with open(output_file, 'w', encoding='utf-8') as outfile:
        json.dump(json_data, outfile, sort_keys=True, indent=4)

    return

# init args parser, read and save sources
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description = "Read models file and clean it resp. add relations.")
    parser.add_argument("-i", "--input", type = str, help = "input file", required = True)
    parser.add_argument("-o", "--output", type = str, help = "output file", required = True)

    args = parser.parse_args()

    try:

        api_model = read_model(args.input)
        clean_model = clean_model(api_model)
        save_json_file(clean_model, args.output)

    except Exception as e:
        print()
        print("Error {}: {}".format(type(e).__name__, e))
        print(traceback.format_exc())
