import copy

# clean model
def clean_model(orig_model):

    cleaned_model = copy.deepcopy(orig_model)

    # remove "allOf" models, they are not needed
    drop_list = []
    for model, variables in cleaned_model["models"].items():
        if (model.endswith("AllOf")):
            drop_list.append(model)
    for model in drop_list:
        cleaned_model["models"].pop(model)

    # clean variables in models, store relations
    relation_list = {}
    for model, variables in cleaned_model["models"].items():

        # print("Processing {}...".format(model))

        for name, variable in variables.items():

            # default value
            if (("defaultValue" in variable) and ((variable["defaultValue"] == "") or (variable["defaultValue"] == "null"))):
                variable.pop("defaultValue")

            # data type
            newDataType = None

            if (variable["dataType"] == "UUID"):
                if (variable["complexType"] == "UUID"):
                    newDataType = "UUID"

            if (variable["dataType"] == "String"):
                if (variable["complexType"] == "string"):
                    newDataType = "TEXT"

            if (variable["dataType"] == "Boolean"):
                if (variable["complexType"] == "boolean"):
                    newDataType = "BOOLEAN"

            if (variable["dataType"] == "Integer"):
                if (variable["complexType"] == "integer"):
                    newDataType = "INTEGER"

            if (variable["dataType"] == "Float"):
                if (variable["complexType"] == "float"):
                    newDataType = "NUMERIC"

            if (variable["dataType"] == "URI"):
                if (variable["complexType"] == "URI"):
                    newDataType = "TEXT"

            if (variable["dataType"] == "Date"):
                if (variable["complexType"] == "DateTime"):
                    newDataType = "TIMESTAMP"

            if (variable["dataType"] == "date"):
                if (variable["complexType"] == "date"):
                    newDataType = "DATE"

            if (variable["dataType"] == "List"):
                if (model not in relation_list):
                    relation_list[model] = []
                relation_list[model].append({"referenced_model": variable["complexType"], "variable": name})
                newDataType = "List"

            if (variable["dataType"].startswith("anyOf<")):
                newDataType = "UUID"
                variable["reference"] = "{}(uuid)".format(variable["dataType"][(variable["dataType"].find("<") + 1):variable["dataType"].find(">")])


            # as openapigen does not deliver "geometry" or "time" as complex data type, we convert using property name
            # shame on us
            if (name == "coordinate"):
                newDataType = "GEOMETRY"

            if (name.endswith("Time")):
                newDataType = "TIME"


            if (newDataType is not None):

                variable["dataType"] = newDataType
                variable.pop("complexType")

            else:
                print("Did not clean dataType '{}'.'{}' with complexType '{}'".format(model, variable["dataType"], variable["complexType"]))

    # process relations
    for model, references in relation_list.items():

        # print("Processing list reference for {}...".format(model))

        if (model in cleaned_model["models"]):

            for reference in references:

                nmRelationTable = "nm{}{}".format(model, reference["referenced_model"])
                cleaned_model["models"][nmRelationTable] = {}

                cleaned_model["models"][nmRelationTable]["fk_{}_uuid".format(model)] = {
                    "dataType": "uuid",
                    "primaryKey": False,
                    "required": True,
                    "reference": "{}(uuid)".format(model)
                }

                cleaned_model["models"][nmRelationTable]["fk_{}_uuid".format(reference["referenced_model"])] = {
                    "dataType": "uuid",
                    "primaryKey": False,
                    "required": True,
                    "reference": "{}(uuid)".format(reference["referenced_model"])
                }

                cleaned_model["models"][model].pop(reference["variable"])

        else:
            print("Model '{}' not existing".format(model))

    return cleaned_model
