# KiezRadar - Backend

Dieses Repository enthält das KiezRadar-Backend.

Das Handbuch für Entwickler finden Sie unter https://kiezradar.gitlab.io/backend (Englisch)

Developer documentation: https://kiezradar.gitlab.io/backend

- [changelog](changelog.md)

## Gefördert durch

[![Logo "Der Regierende Bürgermeister - Senatskanzlei"](rbmskzl.png "Der Regierende Bürgermeister - Senatskanzlei")](https://www.berlin.de/rbmskzl/)

## Copyright

Copyright 2021-2022 KiezRadar <kiezradar@fokus.fraunhofer.de>

This program is free software: you can redistribute it and/or modify
it under the terms of the Lesser GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Lesser GNU General Public License for more details.

You should have received a copy of the Lesser GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
