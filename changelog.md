# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- API examples in documentation

### Changed

- updated imported data
- using new nominatim docker 4.0
- removed sudo from docker call scripts by using `--user "$(id -u):$(id -g)"`

### Removed

- own nominatim docker image and submodule

### Fixed

- added exec flag for scripts


## [0.2.0] - 2021-04-29

### Changed

- new placeholder


## [0.1.0] - 2021-04-16

Initial commit with basic functionality.
