import copy
import json

from datetime import datetime

from source_helper import add_location_hints, download_data, get_uuid, get_url_from_dict

def process_source(metadata, depth, is_verbose):

    if (is_verbose):
        print("{}{} - URL: '{}'".format("\t" * depth, metadata["source_title"], metadata["url"]))

    raw_data = download_data(metadata["url"])
    if (is_verbose):
        print("{}download: success.".format("\t" * (depth + 1)))

    json_data = mbplans_to_json(raw_data, metadata)
    if (is_verbose):
        print("{}conversion xml -> json: success.".format("\t" * (depth + 1)))

    return json_data


# converts meinBerlin plans to json
def mbplans_to_json(raw_data, metadata):

    event_list = []

    json_data = json.loads(raw_data)

    for event in json_data:

        event_metadata = copy.deepcopy(metadata)
        event_metadata["uuid"] = get_uuid(event["url"], event["title"], event_metadata["source_title"])

        event_metadata["title"] = event["title"].strip()

        if (("description" in event) and (event["description"] is not None) and (event["description"].strip() != "")):
            event_metadata["description"] = event["description"]

        timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        event_metadata["createdat"] = event["created_or_modified"][0:10]
        event_metadata["updatedat"] = event_metadata["createdat"]


        event_metadata["startdate"] = event_metadata["createdat"]
        event_metadata["enddate"] = event_metadata["startdate"]
        if (("active_phase" in event) and (event["active_phase"] is not None) and isinstance(event["active_phase"], list)):
            event_metadata["enddate"] = event["active_phase"][2][0:10]

        if (("tile_image" in event) and (event["tile_image"] is not None) and (event["tile_image"].strip())):
            event_metadata["image"] = {
                "createdat": timestamp,
                "filename": event["tile_image"][(event["tile_image"].rfind('/') + 1):].strip(),
                "title": event_metadata["title"],
                "updatedat": timestamp,
                "url": event["tile_image"].strip(),
                "uuid": event_metadata["uuid"]
            }

        if (("cost" in event) and (event["cost"] is not None) and (event["cost"].strip() != "")):
            event_metadata["budget"] = event["cost"]

        event_metadata["projectauthority"] = event["organisation"]

        if ("topics" in event):
            for topic in event["topics"]:
                event_metadata["section_keys"].append(topic)

        if ("district" in event):
            event_metadata["district_key"] = event["district"]
        else:
            print("kein district in {}".format(event_metadata["uuid"]))

        if (("point" not in event) or (event["point"] is None) or (not event["point"])):
            if (("point_label" in event) and (event["point_label"] is not None) and (event["point_label"].strip() != "")):
                add_location_hints(event_metadata["location"]["hint"], event["point_label"].strip())
                event_metadata["location"]["title"] = event["point_label"].strip()
            if (("organisation" in event) and (event["organisation"] is not None) and (event["organisation"].strip() != "")):
                add_location_hints(event_metadata["location"]["hint"], event["organisation"].strip())
                event_metadata["location"]["title"] = event["organisation"].strip()
        else:
            if (event["point"]["geometry"]["type"] == "Point"):
                event_metadata["location"] = {
                    "geometry": {
                        "coordinates": {
                            "lat": event["point"]["geometry"]["coordinates"][1],
                            "lon": event["point"]["geometry"]["coordinates"][0]
                        },
                        "type": event["point"]["geometry"]["type"]
                    }
                }
                event_metadata["location"]["title"] = event["point_label"].strip()
            else:
                raise Exception("Unknown handling of geometry type {} in {}".format(event["point"]["geometry"]["type"], event_metadata["uuid"]))


        if (("url" in event) and (event["url"] is not None)):
            if (event["url"].startswith("/")):
                urlparts = event_metadata["url_parts"]
                urlparts["query"] = {}
                urlparts["path"] = {"endpoint": event["url"]}
                moreurl = get_url_from_dict(urlparts)
            else:
                moreurl = event["url"]

            event_metadata["morelinks_urls"].append({"title": "Weitere Informationen", "url": moreurl})
            event_metadata["participationurl"] = moreurl

        if (("plan_url" in event) and (event["plan_url"] is not None)):
            if (event["plan_url"].startswith("/")):
                urlparts = event_metadata["url_parts"]
                urlparts["query"] = {}
                urlparts["path"] = {"endpoint": event["plan_url"]}
                moreurl = get_url_from_dict(urlparts)
            else:
                moreurl = event["plan_url"]

            event_metadata["morelinks_urls"].append({"title": "Vorhaben", "url": moreurl})

        event["kiezradar"] = event_metadata
        event_list.append(event)

    return event_list
