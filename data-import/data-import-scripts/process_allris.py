import convertdate
import copy
import xmltodict

from datetime import datetime

from source_helper import download_data, get_uuid, get_url_from_dict

# converts XML to json
def process_source(metadata, depth, is_verbose):

    event_list = []

    for year in range(int(metadata["url_parts"]["query"]["YY"]["from"]), int(metadata["url_parts"]["query"]["YY"]["to"]) + 1):

        for month in range(int(metadata["url_parts"]["query"]["MM"]["from"]), int(metadata["url_parts"]["query"]["MM"]["to"]) + 1):

            month_metadata = copy.deepcopy(metadata)
            month_metadata["url_parts"]["query"]["MM"] = str(month)
            month_metadata["url_parts"]["query"]["YY"] = str(year)
            month_metadata["url"] = get_url_from_dict(month_metadata["url_parts"])

            event_list += process_single_source(month_metadata, depth, is_verbose)

    return event_list

# converts XML to json
def process_single_source(metadata, depth, is_verbose):

    if (is_verbose):
        print("{}{} - URL: '{}'".format("\t" * depth, metadata["source_title"], metadata["url"]))

    raw_data = download_data(metadata["url"])
    if (is_verbose):
        print("{}download: success.".format("\t" * (depth + 1)))

    json_data = allris_to_json(raw_data, metadata, depth, is_verbose)
    if (is_verbose):
        print("{}conversion xml -> json: success.".format("\t" * (depth + 1)))

    return json_data

# converts XML to json
def allris_to_json(raw_data, metadata, depth, is_verbose):

    json_data = xmltodict.parse(raw_data)

    event_list = []

    if ("xml" in json_data):

        if (metadata["url_parts"]["path"]["endpoint"] == "to010.asp"):

            event_metadata = copy.deepcopy(metadata)
            event_metadata["uuid"] = get_uuid(json_data["xml"]["head"]["silfdnr"], event_metadata["source_title"])
            event_metadata["source_type"] = "allris_to"

            event_metadata["title"] = json_data["xml"]["head"]["sitext"].strip()

            event_metadata["createdat"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            event_metadata["updatedat"] = event_metadata["createdat"]

            event_metadata["startdate"] = convertdate.julianday.to_datetime(int(json_data["xml"]["head"]["sidat"])).strftime("%Y-%m-%d")
            if (("sisb" in json_data["xml"]["special"]) and (json_data["xml"]["special"]["sisb"] is not None) and (json_data["xml"]["special"]["sisb"].strip() != "")):
                event_metadata["starttime"] = "{}:00".format(json_data["xml"]["special"]["sisb"])
            event_metadata["enddate"] = event_metadata["startdate"]
            if (("sise" in json_data["xml"]["special"]) and (json_data["xml"]["special"]["sise"] is not None) and (json_data["xml"]["special"]["sise"].strip() != "")):
                event_metadata["endtime"] = "{}:00".format(json_data["xml"]["special"]["sise"])

            event_metadata["district_key"] = event_metadata["source_title"].replace("Bezirksamt", "").strip()

            if (("raort" in json_data["xml"]["head"]) and (json_data["xml"]["head"]["raort"] is not None) and (json_data["xml"]["head"]["raort"].strip() != "")):
                event_metadata["location"]["title"] = json_data["xml"]["head"]["raort"].strip()
                event_metadata["location"]["hint"].append(json_data["xml"]["head"]["raort"].strip())
            if (("ratext" in json_data["xml"]["head"]) and (json_data["xml"]["head"]["ratext"] is not None) and (json_data["xml"]["head"]["ratext"].strip() != "")):
                event_metadata["location"]["title"] = json_data["xml"]["head"]["ratext"].strip()

            json_data["kiezradar"] = event_metadata
            event_list.append(json_data)

        else:
            raise Exception("Unknown handling of to010.asp source: '{}'.".format(metadata["url"]))

    elif ("kalender" in json_data):

        if ((json_data["kalender"] is not None) and ("sitzung" in json_data["kalender"])):

            # if there is only one entry in "kalender", there is no list but only the entry
            # took me a while to figure this out...
            if (not isinstance(json_data["kalender"]["sitzung"], list)):
                json_data["kalender"]["sitzung"] = [json_data["kalender"]["sitzung"]]

            for meeting in json_data["kalender"]["sitzung"]:

                if (meeting["links"]["tagesordnung"] is None):

                    event_metadata = copy.deepcopy(metadata)
                    event_metadata["uuid"] = get_uuid(meeting["id"], event_metadata["source_title"])
                    event_metadata["source_type"] = "allris_si"

                    event_metadata["title"] = meeting["titel"].strip()

                    event_metadata["createdat"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                    event_metadata["updatedat"] = event_metadata["createdat"]

                    event_metadata["startdate"] = "{}-{}-{}".format(meeting["datum"]["@jjjj"].strip(), meeting["datum"]["@mm"].strip(), meeting["datum"]["@tt"].strip())
                    if (("@von" in meeting["zeit"]) and (meeting["zeit"]["@von"] is not None) and (meeting["zeit"]["@von"].strip() != "")):
                        event_metadata["starttime"] = "{}:00".format(meeting["zeit"]["@von"].strip())
                    event_metadata["enddate"] = event_metadata["startdate"]
                    if (("@bis" in meeting["zeit"]) and (meeting["zeit"]["@bis"] is not None) and (meeting["zeit"]["@bis"].strip() != "")):
                        event_metadata["endtime"] = "{}:00".format(meeting["zeit"]["@bis"].strip())

                    event_metadata["section_keys"] = {}

                    event_metadata["district_key"] = event_metadata["source_title"].replace("Bezirksamt", "").strip()

                    if (("@gebaeude" in meeting["ort"]) and (meeting["ort"]["@gebaeude"] is not None) and (meeting["ort"]["@gebaeude"].strip() != "")):
                        event_metadata["location"]["hint"].append(meeting["ort"]["@gebaeude"].strip())
                    if (("@raum" in meeting["ort"]) and (meeting["ort"]["@raum"] is not None) and (meeting["ort"]["@raum"].strip() != "")):
                        event_metadata["location"]["hint"].append(meeting["ort"]["@raum"].strip())
                    if (("@strasse" in meeting["ort"]) and (meeting["ort"]["@strasse"] is not None) and (meeting["ort"]["@strasse"].strip() != "")):
                        event_metadata["location"]["hint"].append(meeting["ort"]["@strasse"].strip())

                    if (event_metadata["location"]["hint"]):
                        event_metadata["location"]["title"] = ", ".join(event_metadata["location"]["hint"])

                    meeting["kiezradar"] = event_metadata
                    event_list.append(meeting)

                else:

                    event_metadata = copy.deepcopy(metadata)
                    event_metadata["url_parts"]["path"]["endpoint"] = "to010.asp"
                    event_metadata["url_parts"]["query"] = {"selfaction": "ws", "SILFDNR": meeting["id"]}
                    event_metadata["url"] = get_url_from_dict(event_metadata["url_parts"])
                    event_list += process_single_source(event_metadata, depth + 2, is_verbose)

    else:

        raise Exception("Unknown handling of source: '{}'.".format(metadata["url"]))

    return event_list
