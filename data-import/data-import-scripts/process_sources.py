import copy
import json
import os
import traceback
import urllib
import urllib3
import uuid

from source_helper import get_uuid, get_url_from_dict, save_json_file

import process_allris
import process_mbplans
import process_qmava

# process sources: read, convert and write
def process_sources(sources, config, sections, eventtypes, districts, output_path, is_verbose):

    save_sections(sections, output_path, 1, is_verbose)
    save_eventtypes(eventtypes, output_path, 1, is_verbose)
    save_districts(districts, output_path, 1, is_verbose)

    for source_type, urls in sources.items():

        print("Processing {} sources (this takes a while - an hour or so?)...".format(source_type))

        for source_def in urls:

            if (source_def["active"]):

                metadata = {
                    "source_title": source_def["title"],
                    "source_type": source_type,
                    "url": get_url_from_dict(source_def["url"]),
                    "url_parts": source_def["url"],
                    "section_keys": [],
                    "morelinks_urls": [],
                    "location": {"hint": []},
                    "eventtype_uuid": eventtypes[source_def["eventtype"]]["uuid"]
                }

                try:

                    if (source_type == "allris"):
                        event_list = process_allris.process_source(metadata, 1, is_verbose)

                    elif (source_type == "qmava"):
                        event_list = process_qmava.process_source(metadata, 1, is_verbose)

                    elif (source_type == "mbplans"):
                        event_list = process_mbplans.process_source(metadata, 1, is_verbose)

                    elif (source_type == "mbprojects"):
                        # same processor as plans for the moment
                        event_list = process_mbplans.process_source(metadata, 1, is_verbose)

                    else:
                        raise Exception("Unknown source type '{}'".format(source_type))

                    add_event_info(event_list, config, sections, districts, source_def["slot"])
                    save_events(event_list, output_path, 1, is_verbose)

                except Exception as e:
                    print("Error {} processing {}: {}".format(type(e).__name__, source_type, e))
                    print(traceback.format_exc())

            else:
                print("Source {} skipped: not active.".format(source_def["title"]))


    return

# add generic event info
def add_event_info(event_list, config, sections, districts, slot):

    for event in event_list:

        # api-links
        event["kiezradar"]["apilinks"] = {
            "uuid": get_uuid("apilinks", event["kiezradar"]["uuid"]),
            "api_url": "{}/{}".format(config["baseURL"], config["APIURL"]),
            "links": {}
        }
        event["kiezradar"]["apilinks"]["links"]["list"] = {
            "url": "/{}".format(slot.lower()),
            "uuid": get_uuid("list", "/{}".format(slot.lower()))
        }
        event["kiezradar"]["apilinks"]["links"]["self"] = {
            "url": "/{}?uuid=eq.{}".format(slot.lower(), event["kiezradar"]["uuid"]),
            "uuid": get_uuid("self", "/{}?uuid=eq.{}".format(slot.lower(), event["kiezradar"]["uuid"]))
        }

        # more links
        if (event["kiezradar"]["morelinks_urls"]):
            event["kiezradar"]["morelinks"] = {"uuid": get_uuid("morelinks", event["kiezradar"]["uuid"]), "links": {}}

            for url in event["kiezradar"]["morelinks_urls"]:
                event["kiezradar"]["morelinks"]["links"][url["title"]] = {
                    "url": url["url"],
                    "uuid": get_uuid(url["title"], url["url"])
                }

        event["kiezradar"].pop("morelinks_urls")

        # sections
        event["kiezradar"]["sections"] = []
        for key in event["kiezradar"]["section_keys"]:

            found = False

            for section, details in sections.items():

                if (key in details["allocation"]):
                    found = True
                    if (details["uuid"] not in event["kiezradar"]["sections"]):
                        event["kiezradar"]["sections"].append(details["uuid"])

            if (not found):
                print("Section '{}' of '{}' missing in sections list.".format(key, event["kiezradar"]["source_title"]))

        event["kiezradar"].pop("section_keys")

        # sections
        if ("district_key" in event["kiezradar"]):

            for district, details in districts.items():

                if (event["kiezradar"]["district_key"] in details["allocation"]):
                    event["kiezradar"]["district"] = details["uuid"]

            if ("district" not in event["kiezradar"]):
                print("District key '{}' of '{}' missing in district list.".format(event["kiezradar"]["district_key"], event["kiezradar"]["source_title"]))

            event["kiezradar"].pop("district_key")

    return

# save events to files
def save_events(event_list, output_path, depth, is_verbose):

    for event in event_list:

        filename = os.path.join(os.getcwd(), output_path, event["kiezradar"]["source_type"], "{}_{}.json".format(event["kiezradar"]["source_type"], event["kiezradar"]["uuid"]))
        save_json_file(event, filename, depth, is_verbose)

    return

# save sections
def save_sections(sections, output_path, depth, is_verbose):

    sections_out = {'sections': copy.deepcopy(sections)}
    for section, details in sections_out["sections"].items():
        details.pop("allocation")

    filename = os.path.join(os.getcwd(), output_path, "sections.json")
    save_json_file(sections_out, filename, depth, is_verbose)

    return

# save eventtypes
def save_eventtypes(eventtypes, output_path, depth, is_verbose):

    eventtypes_out = {'eventtypes': copy.deepcopy(eventtypes)}

    filename = os.path.join(os.getcwd(), output_path, "eventtypes.json")
    save_json_file(eventtypes_out, filename, depth, is_verbose)

    return

# save districts
def save_districts(districts, output_path, depth, is_verbose):

    districts_out = {'districts': copy.deepcopy(districts)}
    for district, details in districts_out["districts"].items():
        details.pop("allocation")

    filename = os.path.join(os.getcwd(), output_path, "districts.json")
    save_json_file(districts_out, filename, depth, is_verbose)

    return
