import argparse
import json
import os

from datetime import datetime

from source_helper import download_geoloc, save_json_file

def geoloc_data(filecontent):

    if ("geometry" not in filecontent["kiezradar"]["location"]):
        for location_hint in filecontent["kiezradar"]["location"]["hint"]:
            geoloc = json.loads(download_geoloc(location_hint))
            if (geoloc):
                filecontent["kiezradar"]["location"]["geometry"] = {
                    "coordinates": {
                        "lat": geoloc[0]["lat"],
                        "lon": geoloc[0]["lon"]
                    },
                    "type": "Point"
                }
                filecontent["kiezradar"]["location"]["geocodingtitle"] = location_hint
                if ("title" not in filecontent["kiezradar"]["location"]):
                    filecontent["kiezradar"]["location"]["title"] = filecontent["kiezradar"]["location"]["hint"]
                break

    if ("title" not in filecontent["kiezradar"]["location"]):
        filecontent["kiezradar"].pop("location")

    if ("location" in filecontent["kiezradar"]):
        timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        filecontent["kiezradar"]["location"]["createdat"] = timestamp
        filecontent["kiezradar"]["location"]["updatedat"] = timestamp

    return(filecontent)

# process all files
def process_files(input_path, output_path, is_verbose):

    for directory in os.listdir(os.path.join(os.getcwd(), input_path)):
        if os.path.isdir(os.path.join(os.getcwd(), input_path, directory)):
            for filename in os.listdir(os.path.join(os.getcwd(), input_path, directory)):

                absfilename = os.path.join(os.getcwd(), input_path, directory, filename)
                if os.path.isfile(absfilename):

                    if (is_verbose):
                        print("Processing '{}'.".format(absfilename))

                    with open(absfilename, 'r') as file:
                        geoloc_filecontent = geoloc_data(json.load(file))
                        output_filename = os.path.join(os.getcwd(), output_path, directory, filename)
                        save_json_file(geoloc_filecontent, output_filename, 0, is_verbose)

                else:
                    print("error: {}".format(absfilename))

    return

# init args parser, read and save sources
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description = "Geoloc KiezRadar data files.")
    parser.add_argument("-i", "--input", type = str, help = "input path", required = True)
    parser.add_argument("-o", "--output", type = str, help = "output path", required = True)
    parser.add_argument("-v", "--verbose", action='store_true', help = "verbose output", required = False)

    args = parser.parse_args()

    try:

        process_files(args.input, args.output, args.verbose)

    except Exception as e:
        print()
        print("Error {}: {}".format(type(e).__name__, e))
