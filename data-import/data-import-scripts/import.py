import argparse
import json
import os

from source_helper import get_uuid, read_json_file
from process_sources import process_sources

# read and return config
def read_config(filename, is_verbose):

    json_content = read_json_file(filename, 0, is_verbose)

    print("Config read.")
    print()

    return json_content

# read sources, return map of sources
def read_sources(filename, config, is_verbose):

    json_content = read_json_file(filename, 0, is_verbose)

    print("Sources read.")
    if (not bool(json_content)):
        print("\tno sources.")
    for type, urls in json_content.items():
        print("\t{} sources: {} URLs to process.".format(type, len(urls)))
    print()

    return json_content

# read sections, return map of sections
def read_sections(filename, config, is_verbose):

    json_content = read_json_file(filename, 0, is_verbose)

    sections = {}
    for section, keys in json_content.items():
        sections[section] = {
            "uuid": get_uuid(section),
            "allocation": keys
        }

    print("Sections read.")
    print()

    return sections

# read event types, return map of event types
def read_eventtypes(filename, config, is_verbose):

    json_content = read_json_file(filename, 0, is_verbose)

    eventtypes = {}
    for eventtype, keys in json_content.items():
        eventtypes[eventtype] = keys
        eventtypes[eventtype]["uuid"] = get_uuid(eventtype)
        eventtypes[eventtype]["url"] = "{}/{}/{}".format(config["baseURL"], config["pathPlaceholder"], keys["filename"])

    print("Event types read.")
    print()

    return eventtypes

# read districts, return map of districts
def read_districts(filename, config, is_verbose):

    json_content = read_json_file(filename, 0, is_verbose)

    districts_geo = []
    for geosource in json_content["geosource"]:
        geo_input_file = os.path.join(os.path.dirname(filename), geosource["file"])
        if not os.path.isfile(geo_input_file):
            raise FileNotFoundError("File '{}' does not exist.".format(geo_input_file))

        with open(geo_input_file, 'r') as file:
            districts_geo.append(json.load(file))

    districts = {}
    for district, keys in json_content["allocation"].items():
        for geosource in districts_geo:
            for feature in geosource["features"]:

                if ("Gemeinde_n" in feature["properties"]):
                    name = feature["properties"]["Gemeinde_n"]
                elif ("name_1" in feature["properties"]):
                    name = feature["properties"]["name_1"]

                if (name == district):
                    if (district in districts):
                        print("Double disctrict '{}'.".format(district))
                    districts[district] = {"uuid": get_uuid(district), "geoloc": feature["geometry"], "allocation": keys}

    print("Districts read.")
    print()

    return districts


# init args parser, read and save sources
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description = "Get KiezRadar data and store it to json files.")
    parser.add_argument("-c", "--config", type = str, help = "config file", required = True)
    parser.add_argument("-s", "--sources", type = str, help = "source file", required = True)
    parser.add_argument("-e", "--sections", type = str, help = "sections file", required = True)
    parser.add_argument("-t", "--eventtypes", type = str, help = "event types file", required = True)
    parser.add_argument("-d", "--districts", type = str, help = "districts file", required = True)
    parser.add_argument("-o", "--output", type = str, help = "output path", required = True)
    parser.add_argument("-v", "--verbose", action='store_true', help = "verbose output", required = False)

    args = parser.parse_args()

    try:

        config = read_config(args.config, args.verbose)
        sources = read_sources(args.sources, config, args.verbose)
        sections = read_sections(args.sections, config, args.verbose)
        eventtypes = read_eventtypes(args.eventtypes, config, args.verbose)
        districts = read_districts(args.districts, config, args.verbose)
        process_sources(sources, config, sections, eventtypes, districts, args.output, args.verbose)

    except Exception as e:
        print()
        print("Error {}: {}".format(type(e).__name__, e))
