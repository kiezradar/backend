import copy
import icalendar
import json
import recurring_ical_events

from datetime import datetime, timedelta, date

from source_helper import add_location_hints, download_data, get_uuid


def process_source(metadata, depth, is_verbose):

    if (is_verbose):
        print("{}{} - URL: '{}'".format("\t" * depth, metadata["source_title"], metadata["url"]))

    raw_data = download_data(metadata["url"])
    if (is_verbose):
        print("{}download: success.".format("\t" * (depth + 1)))

    json_data = ical_to_json(raw_data, metadata)
    if (is_verbose):
        print("{}conversion xml -> json: success.".format("\t" * (depth + 1)))

    return json_data


# converts ical to json (ical of joomly for now)
def ical_to_json(ical_data, metadata):

    ical = icalendar.Calendar.from_ical(ical_data)

    today = datetime.now()

    events = ical.walk("VEVENT") + recurring_ical_events.of(ical).between(today, today + timedelta(days=366))

    event_list = {}

    for event in events:

        event_json = json.loads(json.dumps(event, cls=ICalendarEncoder))

        event_metadata = copy.deepcopy(metadata)
        event_metadata["uuid"] = "-".join([event["UID"][0:8], event["UID"][8:12], event["UID"][12:16], event["UID"][16:20], event["UID"][20:32]])

        timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        event_metadata["createdat"] = timestamp
        event_metadata["updatedat"] = timestamp

        event_metadata["title"] = event_json["SUMMARY"].strip()


        if ("DESCRIPTION" in event_json):
            event_metadata["description"] = event_json["DESCRIPTION"].strip()

        date_text = event_json["DTSTART"].strip()
        if (len(date_text) == 8):
            event_metadata["startdate"] = datetime.strptime(date_text, '%Y%m%d').date().isoformat()
        elif (len(date_text) == 15):
            event_metadata["startdate"] = datetime.strptime(date_text, '%Y%m%dT%H%M%S').date().isoformat()
            event_metadata["starttime"] = datetime.strptime(date_text, '%Y%m%dT%H%M%S').time().isoformat()
        else:
            raise Exception("Unbekanntes Datumformat '{}'".format(date_text))

        event_metadata["enddate"] = event_metadata["startdate"]
        if ("DTEND" in event_json):
            date_text = event_json["DTEND"].strip()
            if (len(date_text) == 8):
                event_metadata["enddate"] = datetime.strptime(date_text, '%Y%m%d').date().isoformat()
            elif (len(date_text) == 15):
                event_metadata["enddate"] = datetime.strptime(date_text, '%Y%m%dT%H%M%S').date().isoformat()
                event_metadata["endtime"] = datetime.strptime(date_text, '%Y%m%dT%H%M%S').time().isoformat()
            else:
                raise Exception("Unbekanntes Datumformat '{}' ({}).".format(date_text, event_metadata["uuid"]))

        if ("CATEGORIES" in event_json):
            for category in event_json["CATEGORIES"].split(','):
                event_metadata["section_keys"].append(category.strip())

        if ("LOCATION" in event_json):
            event_metadata["location"]["title"] = event_json["LOCATION"].strip()
            add_location_hints(event_metadata["location"]["hint"], event_json["LOCATION"].strip())

        event_metadata["district_key"] = "Reinickendorf"

        if (event_metadata["uuid"] in event_list):
            event_metadata["series"] = event_metadata["uuid"]
            event_metadata["uuid"] = get_uuid(event_metadata["uuid"], event_metadata["startdate"])

        event_json["kiezradar"] = event_metadata
        event_list[event_json["kiezradar"]["uuid"]] = event_json

    return list(event_list.values())

# ical encoder, needed, because some data types are bytes and dates which cannot be transformed automatically
class ICalendarEncoder(json.JSONEncoder):
    def default(self, obj, markers=None):

        try:
            if obj.__module__.startswith("icalendar.prop"):
                return (obj.to_ical())
        except AttributeError:
            pass

        if isinstance(obj, datetime):
            return (obj.now().strftime('%Y-%m-%dT%H:%M:%S'))

        if isinstance(obj, bytes):
            return obj.decode("utf-8")

        return json.JSONEncoder.default(self,obj)
