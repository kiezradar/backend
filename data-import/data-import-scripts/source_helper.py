import json
import os
import urllib
import urllib3
import uuid

# returns raw data of url
def download_data(url):

    http = urllib3.PoolManager()
    response = http.request('GET', url)

    return response.data

# returns geloc information for place
def download_geoloc(address):

    http = urllib3.PoolManager()
    response = http.request(
        'GET',
        'http://nominatim:8080/search.php',
        {
            'q': address,
            'format': 'json',
            'polygon': '1'
        }
    )

    return response.data

# return id from given values
def get_uuid(*parts):

    if (len(parts) > 0):
        value = "-".join(parts)
    else:
        value = "abcdedcba"

    return str(uuid.uuid5(uuid.NAMESPACE_DNS, value))

# return URL string from dict
def get_url_from_dict(dict_url):

    # quick and very dirty creating of path string
    urlpath = ""
    for key, value in dict_url["path"].items():
        urlpath = urllib.parse.urljoin(urlpath, value)

    # quick and very dirty creating of query string
    urlquery = ""
    for key, value in dict_url["query"].items():
        urlquery += "{}={}&".format(key, value)
    urlquery = urlquery[:-1]

    return urllib.parse.urlunsplit((
        dict_url["scheme"],
        dict_url["netloc"],
        urlpath,
        urlquery,
        ""
    ))

# read json file
def read_json_file(filename, depth, is_verbose):

    if (is_verbose):
        print("{}reading from '{}'".format("\t" * (depth), filename))

    input_file = os.path.join(os.getcwd(), filename)

    if not os.path.isfile(input_file):
        raise FileNotFoundError("File '{}' does not exist.".format(input_file))

    if (is_verbose):
        print("{}which is '{}'".format("\t" * (depth + 1), input_file))

    with open(input_file, 'r') as file:
        json_raw = json.load(file)

    if (is_verbose):
        print("{}read json content successfully.".format("\t" * (depth + 1)))

    return json_raw

# save json file
def save_json_file(json_data, filename, depth, is_verbose):

    if (is_verbose):
        print("{}saving to '{}'".format("\t" * (depth + 1), filename))
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    with open(filename, 'w', encoding='utf-8') as outfile:
        json.dump(json_data, outfile, sort_keys=True, indent=4)

    return

# add location hints, split location if possible
def add_location_hints(hint_array, location):

    hint_array.append(location)

    add_location_hints_separated(hint_array, location, ":")
    add_location_hints_separated(hint_array, location, ",")
    add_location_hints_separated(hint_array, location, "(")
    add_location_hints_separated(hint_array, location, " ")

    return

# add location hints, split location at separator
def add_location_hints_separated(hint_array, location, separator):

    location_parts = location.split(separator, 1)
    if (len(location_parts) > 1):
        hint_array.append(location_parts[1].strip())
        add_location_hints_separated(hint_array, location_parts[1].strip(), separator)

    return
