#!/bin/bash

source ../variables.sh

CONTAINERNAME=$CONTAINER_PYTHON
IMAGENAME=$IMAGE_PYTHON

WORKDIR=/usr/src/myapp

OUTPATH=$DATA_PATH/$DATA_PATH_OUT
OUTPATH_GEOLOC=$DATA_PATH/$DATA_PATH_OUT_GEOLOC

echo "Geoloc data"
echo

rm --recursive $OUTPATH_GEOLOC/*

docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--interactive \
	--tty \
	--name $CONTAINERNAME \
	--network $NETWORK_NAME \
	--link $IMAGE_NOMINATIM \
	--volume "${PWD}":$WORKDIR \
	--workdir $WORKDIR \
	$IMAGENAME \
		data-import-scripts/geoloc.py \
			--input $OUTPATH \
			--output $OUTPATH_GEOLOC
			#--verbose
