#!/bin/bash

source ../variables.sh

CONTAINERNAME=$CONTAINER_PYTHON
IMAGENAME=$IMAGE_PYTHON

WORKDIR=/usr/src/myapp

CONFIG=$DATA_PATH/$DATA_PATH_IN/base-config.json
SOURCES=$DATA_PATH/$DATA_PATH_IN/sources.json
SECTIONS=$DATA_PATH/$DATA_PATH_IN/sections.json
EVENTTYPES=$DATA_PATH/$DATA_PATH_IN/eventtypes.json
DISTRICTS=$DATA_PATH/$DATA_PATH_IN/districts.json
OUTPATH=$DATA_PATH/$DATA_PATH_OUT

echo "Import data from given sources"
echo

rm --recursive $OUTPATH/*

docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--interactive \
	--tty \
	--name $CONTAINERNAME \
	--volume "${PWD}":$WORKDIR \
	--workdir $WORKDIR \
	$IMAGENAME \
		data-import-scripts/import.py \
			--config $CONFIG \
			--sources $SOURCES \
			--sections $SECTIONS \
			--eventtypes $EVENTTYPES \
			--districts $DISTRICTS \
			--output $OUTPATH
			#--verbose
