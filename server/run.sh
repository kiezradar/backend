#!/bin/bash
echo "Run PostgREST"
echo

docker-compose up --detach

#  PostgREST: http://localhost:3000/ http://localhost:3000/event
#  database: port 5432
#  swagger-api: http://localhost:8080/

#  stop: ./stop

#  bash in db: docker exec -it server_db_1 bash
#  psql in bash: psql -h localhost -U postgres
#  list databases in psql: \l
#  list tables in psql: \dt+
#  quit psql: \q

#  check running containers: docker container ls -a
