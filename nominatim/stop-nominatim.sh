#!/bin/bash

source ../variables.sh

CONTAINERNAME=$CONTAINER_NOMINATIM
IMAGENAME=$IMAGE_NOMINATIM

echo "Stopping nominatim-server $CONTAINERNAME."
echo

docker container stop $CONTAINERNAME
docker network rm $NETWORK_NAME
