#!/bin/bash

source ../variables.sh

CONTAINERNAME=$CONTAINER_NOMINATIM
IMAGENAME=$IMAGE_NOMINATIM

echo "First start: Initialize and run nominatim-server (takes a while, maybe an hour or so)."
echo "Second start: run nominatim-server."
echo
echo "docker exec --interactive --tty $CONTAINERNAME bash"
echo "check-nominatim.sh"
echo "stop-nominatim.sh"
echo

docker network create $NETWORK_NAME

docker run \
	--rm \
	--shm-size=1g \
	--env PBF_URL=https://download.geofabrik.de/europe/germany/berlin-latest.osm.pbf \
	--env REPLICATION_URL=https://download.geofabrik.de/europe/germany/berlin-updates/ \
	--env IMPORT_WIKIPEDIA=false \
	--env NOMINATIM_PASSWORD=very_secure_password \
	--volume nominatim-data:/var/lib/postgresql/12/main \
	--network $NETWORK_NAME \
	--publish 8080:8080 \
	--name $CONTAINERNAME \
	$IMAGENAME \
	&
