#!/bin/bash

source ../variables.sh

CONTAINERNAME=$CONTAINER_NOMINATIM
IMAGENAME=$IMAGE_NOMINATIM

echo "Checking nominatim-server $CONTAINERNAME"
echo

docker container ls --filter "name=$CONTAINERNAME"
